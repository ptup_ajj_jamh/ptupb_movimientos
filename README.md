# README #

API que permite la manipulación de los movimientos.

ALTA
BAJA
MODIFICACION
CONSULTA

### What is this repository for? ###

El código fuente de este proyecto lo puedes descargar con el siguiente comando:
git clone https://senkyyar@bitbucket.org/ptup_ajj_jamh/ptupb_movimientos.git

### How do I get set up? ###
Se requiere tener instalado el siguiente software:
node
npm

Una vez que se tienen las dependencias, se procede a descargar la aplicación.
git clone https://senkyyar@bitbucket.org/ptup_ajj_jamh/ptupb_movimientos.git

Ya que tenemos el código en nuestro equipos nos movemos a la carpeta descargada y ejecutamos los siguientes comandos:
npm install
npm start

Después de esto ya podrás validar si el API esta corriendo correctamente ejecutando la siguiente prueba:
http://<IP>:<PUERTO>/movimientos
