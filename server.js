var express = require('express'), app = express();
var port = process.env.PORT || 3000;
console.log( 'Puerto: ' + port );
var ambiente = process.env.AMBIENTE || "desarrollo";
console.log( 'Ambiente: ' + ambiente );
var requestjson = require('request-json');
var urlMlab = "https://api.mlab.com/api/1/databases/ptup_" + ambiente + "/collections/movimientos/";
var clienteMlab = requestjson.createClient( urlMlab );
var gt = requestjson.createClient( "http://localhost:4000/gt/usuario/validar" );

var bodyParser = require('body-parser');
app.use( bodyParser.json() );
var cors = require('cors');
app.options( '*', cors() );

const queryString = require( 'query-string' );
var url = require( 'url' );

app.listen( port );
console.log( 'API Movimientos started on: ' + port );

// Uso de mLab (MongoDB)
function createURLMLab( query ){
  query.apiKey="PcmRWAjyg0TLZ2XcxQhsdQxubGIrMTFX";
  return "?" + queryString.stringify( query );
}

app.use( function( req, res, next ) {
  console.log( "URL Invocada: ", req.url );
  console.log( "Validando token: " + req.headers.token );
  if( req.headers.token ){
    var bodyReq = { token : req.headers.token };
    var p = gt.post( "", bodyReq, function( error, response, body ){
      console.log('error:', error);
      console.log('statusCode:', response && response.statusCode);
      console.log('body:', body);
      if( response && ( response.statusCode == 200 ) && body.valido )
        next();
      else
         res.send( { error : "Usted no esta autorizado para ejecutar este servicio." } );
    } );
  } else {
    res.send( { error : "Usted no esta autorizado para ejecutar este servicio." } );
  }
});

app.get( "/movimientos", function( req, res ) {
  console.log( "GET /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  var path = createURLMLab( req.query );
  console.log( "    url -> " + url.resolve( urlMlab, path ) );
  clienteMlab.get( path, req.body, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    if( req.query.c )
      res.send( { count: body } );
    else
      res.send( body );
  } );
} );

app.post( "/movimientos", function( req, res ) {
  console.log( "POST /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  var path = createURLMLab( req.query );
  console.log( "    url -> " + url.resolve( urlMlab, path ) );
  clienteMlab.post( path, req.body, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );

app.put( "/movimientos", function( req, res ) {
  console.log( "PUT /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  var path = createURLMLab( req.query );
  console.log( "    url -> " + url.resolve( urlMlab, path ) );
  clienteMlab.put( path, req.body, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );

app.delete( "/movimientos/:oid", function( req, res ) {
  console.log( "DELETE /movimientos" );
  console.log( "    query -> " + JSON.stringify( req.query ) );
  console.log( "    body -> " + JSON.stringify( req.body ) );
  console.log( "    params -> " + JSON.stringify( req.params ) );
  var path = "" + req.params.oid + createURLMLab( req.query );
  console.log( "    url -> " + url.resolve( urlMlab, path ) );
  clienteMlab.del( path, function( error, response, body ){
    console.log('error:', error);
    console.log('statusCode:', response && response.statusCode);
    console.log('body:', body);
    res.send( body );
  } );
} );
